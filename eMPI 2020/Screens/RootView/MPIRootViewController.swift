//
//  MPIRootViewController.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/11/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit
import SnapKit

class MPIRootViewController: TBaseViewController {

    @IBOutlet weak var contentView: ThemeViewBackgroundNormal!
    @IBOutlet weak var tabHome: UIView!
    @IBOutlet weak var tabCalendar: UIView!
    @IBOutlet weak var tabReport: UIView!
    @IBOutlet weak var tabNoti: UIView!
    @IBOutlet weak var tabApp: UIView!
    @IBOutlet weak var tabSetting: UIView!
    
    @IBOutlet weak var imvHome: UIImageView!
    @IBOutlet weak var imvCalendar: UIImageView!
    @IBOutlet weak var imvReport: UIImageView!
    @IBOutlet weak var imvNoti: UIImageView!
    @IBOutlet weak var imvApp: UIImageView!
    @IBOutlet weak var imvSetting: UIImageView!
    
    private var tabbarVC: UITabBarController? = nil
    
    private lazy var homeNav: TBaseNavigationViewController = {
        return TBaseNavigationViewController(rootViewController: MPIHomeViewController())
    }()
    
    private lazy var meetingNav: TBaseNavigationViewController = {
        return TBaseNavigationViewController(rootViewController: MPIMeetingCalendarViewController())
    }()
    
    private lazy var reportNav: TBaseNavigationViewController = {
        return TBaseNavigationViewController(rootViewController: MPIReportFolderViewController())
    }()
    
    private lazy var allAppNav: TBaseNavigationViewController = {
       return TBaseNavigationViewController(rootViewController: MPIAllAppScreenViewController())
    }()
    
    private lazy var notiNav: TBaseNavigationViewController = {
       return TBaseNavigationViewController(rootViewController: MPINotificationViewController())
    }()
    
    private lazy var settingNav: TBaseNavigationViewController = {
       return TBaseNavigationViewController(rootViewController: MPISettingViewController())
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpTabVC()
        reloadTabbar()
    }
    
    func setUpView() {
        weak var weakSelf = self
        tabHome.addAction {
            weakSelf?.doShowScreen(weakSelf?.homeNav)
        }
        
        tabCalendar.addAction {
            weakSelf?.doShowScreen(weakSelf?.meetingNav)
        }
        
        tabReport.addAction {
            weakSelf?.doShowScreen(weakSelf?.reportNav)
        }
        
        tabNoti.addAction {
            weakSelf?.doShowScreen(weakSelf?.notiNav)
        }
        
        tabApp.addAction {
            weakSelf?.doShowScreen(weakSelf?.allAppNav)
        }
        
        tabSetting.addAction {
            weakSelf?.doShowScreen(weakSelf?.settingNav)
        }
    }
    
    func doShowScreen(_ vc: TBaseNavigationViewController?) {
        if tabbarVC?.selectedViewController != vc {
            tabbarVC?.selectedViewController = vc
            reloadTabbar()
        } else {
            vc?.popViewController(animated: true)
        }
    }
    
    func setUpTabVC() {
        let _tabbarVC = UITabBarController()
        _tabbarVC.setViewControllers([homeNav, meetingNav, reportNav, notiNav, allAppNav, settingNav], animated: false)
        _tabbarVC.view.backgroundColor = .clear
        _tabbarVC.tabBar.isHidden = true
        
        contentView.addSubview(_tabbarVC.view)
        _tabbarVC.view.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        self.addChild(_tabbarVC)
        tabbarVC = _tabbarVC
    }
    
    func reloadTabbar() {
        tabHome.backgroundColor     = .clear
        tabCalendar.backgroundColor = .clear
        tabReport.backgroundColor   = .clear
        tabNoti.backgroundColor     = .clear
        tabApp.backgroundColor      = .clear
        tabSetting.backgroundColor  = .clear
        
        let unSelectColor   = theme.tabbarItemUnselectedColor
        let selecteColor    = theme.navigationItemTintColor
        
        imvHome.kSetImageTinColor(unSelectColor)
        imvCalendar.kSetImageTinColor(unSelectColor)
        imvReport.kSetImageTinColor(unSelectColor)
        imvNoti.kSetImageTinColor(unSelectColor)
        imvApp.kSetImageTinColor(unSelectColor)
        imvSetting.kSetImageTinColor(unSelectColor)
        
        let selectdBG = UIColor.white.withAlphaComponent(0.2)
        
        if tabbarVC?.selectedViewController == homeNav {
            tabHome.backgroundColor = selectdBG
            imvHome.kSetImageTinColor(selecteColor)
            
        } else if tabbarVC?.selectedViewController == reportNav {
            tabReport.backgroundColor = selectdBG
            imvReport.tintColor       = selecteColor
            
        } else if tabbarVC?.selectedViewController == meetingNav {
            tabCalendar.backgroundColor = selectdBG
            imvCalendar.kSetImageTinColor(selecteColor)
            
        } else if tabbarVC?.selectedViewController == notiNav {
            tabNoti.backgroundColor = selectdBG
            imvNoti.kSetImageTinColor(selecteColor)
            
        } else if tabbarVC?.selectedViewController == allAppNav {
            tabApp.backgroundColor = selectdBG
            imvApp.kSetImageTinColor(selecteColor)
            
        } else if tabbarVC?.selectedViewController == settingNav {
            tabSetting.backgroundColor = selectdBG
            imvSetting.kSetImageTinColor(selecteColor)
        }
    }
}
