//
//  MPIAllAppScreenViewController.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIAllAppScreenViewController: UIViewController {

    @IBOutlet weak var colView: UICollectionView!
    private var data: [MPIAppModel] = [MPIAppModel("CCTV", "Camera quan sát", "ic_app_cctv"),
                                       MPIAppModel("Netview", "Hệ thống tổng hợp, phân tích báo chí, mạng xã hội", "ic_app_netview"),
                                       MPIAppModel("Họp", "Hệ thống họp thông minh", "ic_app_meeting")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colView.register(MPIAppCollectionViewCell.nib(), forCellWithReuseIdentifier: "cell")
        colView.alwaysBounceVertical = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension MPIAllAppScreenViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MPIAppCollectionViewCell
        let item = data[indexPath.row]
        cell.billData(item)
        return cell
    }
}

extension MPIAllAppScreenViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberItemInLine: CGFloat = 5.0
        let paddingItem: CGFloat = 10.0
        let padding: CGFloat = paddingItem*numberItemInLine
        let width = (collectionView.frame.width - 32.0 - padding)/numberItemInLine
        return CGSize(width: width, height: 200.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20.0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
