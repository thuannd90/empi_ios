//
//  MPIAppCollectionViewCell.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIAppCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imvThumb: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func billData(_ data: MPIAppModel) {
        lblTitle.text   = data.name
        lblDesc.text    = data.desc
        imvThumb.image  = UIImage(named: data.imageName)
    }
}
