//
//  MPINotificationViewController.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPINotificationViewController: TBaseViewController {

    @IBOutlet weak var listNotiTableView: UITableView!
    @IBOutlet weak var detailTableView: UITableView!
    private var listTableViewSource = MPINotiListTableViewDatasource()
    private var detailTableViewDataSource = MPINotiDetailTableViewDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listTableViewSource.table           = listNotiTableView
        listNotiTableView.dataSource        = listTableViewSource
        listNotiTableView.delegate          = listTableViewSource
        listNotiTableView.separatorColor    = theme.separatorColor
        listTableViewSource.detailDataSource = detailTableViewDataSource
        
        detailTableViewDataSource.table     = detailTableView
        detailTableViewDataSource.rootVC    = self
        
        detailTableView.dataSource          = detailTableViewDataSource
        detailTableView.delegate            = detailTableViewDataSource
        detailTableView.separatorColor      = theme.separatorColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.navigationController?.setNavigationBarHidden(true, animated: true)
       }
}
