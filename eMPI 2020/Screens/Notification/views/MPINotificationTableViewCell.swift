//
//  MPINotificationTableViewCell.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPINotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: ThemeLabelTitle!
    @IBOutlet weak var lblTime: ThemeLabelSubTitle!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func billData(_ data: MPINotificationModel) {
        lblTitle.text   = data.title
        lblTime.text    = "Ngày " + data.time
    }
}
