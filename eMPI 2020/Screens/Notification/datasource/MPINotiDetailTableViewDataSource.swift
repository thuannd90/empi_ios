//
//  MPINotiDetailTableViewDataSource.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPINotiDetailTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    weak var table: UITableView? = nil
    weak var rootVC: UIViewController? = nil
    
    private var data: [TableData] = [TableData(icon: "ic_book_mark",
                                               title: "Tặng bằng khen của Bộ trưởng cho các tập thể, cá nhân đã có thành tích xuất sắc trong xây dựng văn bản quy phạm pháp luật năm 2020",
                                               content: "",
                                               dataType: TableDataType.infor),
                                     TableData(icon: "ic_desc",
                                               title: "",
                                               content: "Theo Quyết định số 1263/QĐ-BKHĐT ngày 11/8/2020 của Bộ Kế hoạch và Đầu tư.",
                                               dataType: TableDataType.infor),
                                     TableData(icon: "ic_clock",
                                               title: "",
                                               content: "10/08/2020",
                                               dataType: TableDataType.infor),
                                     TableData(icon: "ic_att",
                                               title: "File đính kèm",
                                               content: "File đính kèm",
                                               dataType: TableDataType.att)]
    
    func showData(_ noti: MPINotificationModel) {
        let newData: [TableData] = [
        TableData(icon: "ic_book_mark",
                  title: noti.title,
                  content: "",
                  dataType: TableDataType.infor),
        TableData(icon: "ic_desc",
                  title: "",
                  content: noti.content,
                  dataType: TableDataType.infor),
        TableData(icon: "ic_clock",
                  title: "",
                  content: noti.time,
                  dataType: TableDataType.infor),
        TableData(icon: "ic_att",
                  title: "File đính kèm",
                  content: "File đính kèm",
                  dataType: TableDataType.att)]
        
        data = newData
        table?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data[indexPath.row]
        if item.dataType == .att {
            let cell = MPIMeetingDetailAttTableViewCell.dequeCellWithTable(tableView)
            cell.billData(item.title, item.icon)
            
            weak var weakSelf = self
            cell.doc1.addAction {
                weakSelf?.doShowDoc()
            }
                       
            cell.doc2.addAction {
                weakSelf?.doShowDoc()
            }
            return cell
        }
        
        let cell = MPIMeetingDetailTableViewCell.dequeCellWithTable(tableView)
        cell.billData(item.title, item.content, item.icon)
        return cell
    }
       
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func doShowDoc() {
        let vc = MPIFileViewerViewController()
        rootVC?.present(vc, animated: true, completion: nil)
    }
}

private struct TableData {
    var icon: String = ""
    var title: String = ""
    var content: String = ""
    var dataType: TableDataType = TableDataType.infor
}

private enum TableDataType {
    case att
    case infor
}
