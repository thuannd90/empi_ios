//
//  MPINotiListTableViewDatasource.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPINotiListTableViewDatasource: NSObject, UITableViewDataSource, UITableViewDelegate {
    weak var table: UITableView? = nil
    weak var detailDataSource: MPINotiDetailTableViewDataSource? = nil
    
    var data: [MPINotificationModel] = [
        MPINotificationModel("Ngừng các hoạt động vui chơi, giải trí trong những ngày Lễ quốc tang đồng chí nguyên Tổng Bí thư Lê Khả Phiêu",
                             "634/BKHĐT-VP",
                             "16/08/2020"),
    
    MPINotificationModel("Tặng bằng khen của Bộ trưởng cho các tập thể, cá nhân đã có thành tích xuất sắc trong xây dựng văn bản quy phạm pháp luật năm 2020",
                         "Theo quyết định số 1263/QĐ-BKHĐT ngày 11/8/2020 của Bộ Kế hoạch và Đầu tư",
                         "16/08/2020"),
    
    MPINotificationModel("Tăng cường công tách phòng, chống dịch Covid-19",
                         "84/CV-CĐBKHĐT",
                         "16/08/2020"),
    
    MPINotificationModel("Thông báo về việc phun thuốc khử khuẩn phòng, chống dịch bệnh Covid-19 tại 04 trụ sở Bộ kế hoạch và Đầu tư số 6B Hoàng Diệu, số 68 Phan Đình Phùng, số 65 Văn Miếu, Lô D25 Cầu Giấy",
                         "52/TB-VP",
                         "07/08/2020"),
    
    MPINotificationModel("Quyết định về việc bổ nhiệm, bổ nhiệm lại, luân chuyển, điều động, từ chức, miễn nhiệm nhân sự giữ chức vụ lãnh đạo, quản lý của Trung tâm Đổi mới sáng tạo Quốc gia",
                         "Quyết định số 1240/QĐ-BKHĐT",
                         "05/08/2020"),
    
    MPINotificationModel("Tặng bằng khen của Bộ trưởng cho các tập thể, cá nhân đã có thành tích xuất sắc trong xây dựng văn bản quy phạm pháp luật năm 2020",
                         "Theo quyết định số 1263/QĐ-BKHĐT ngày 11/8/2020 của Bộ Kế hoạch và Đầu tư",
                         "16/08/2020"),
    
    MPINotificationModel("Tăng cường công tách phòng, chống dịch Covid-19",
                         "84/CV-CĐBKHĐT",
                         "16/08/2020"),
    
    MPINotificationModel("Thông báo về việc phun thuốc khử khuẩn phòng, chống dịch bệnh Covid-19 tại 04 trụ sở Bộ kế hoạch và Đầu tư số 6B Hoàng Diệu, số 68 Phan Đình Phùng, số 65 Văn Miếu, Lô D25 Cầu Giấy",
                         "52/TB-VP",
                         "07/08/2020"),
    
    MPINotificationModel("Quyết định về việc bổ nhiệm, bổ nhiệm lại, luân chuyển, điều động, từ chức, miễn nhiệm nhân sự giữ chức vụ lãnh đạo, quản lý của Trung tâm Đổi mới sáng tạo Quốc gia",
                         "Quyết định số 1240/QĐ-BKHĐT",
                         "05/08/2020")]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MPINotificationTableViewCell.dequeCellWithTable(tableView)
        cell.selectionStyle = .none
        cell.billData(data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        detailDataSource?.showData(data[indexPath.row])
    }
}
