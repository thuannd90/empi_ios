//
//  MPIHomeViewController.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/11/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIHomeViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.backgroundColor = theme.tableBackgroundColor
        webView.scalesPageToFit = true
        loadReport()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func loadReport() {
        let urlStr = "http://pbi.mpi.gov.vn/reports/powerbi/UAT/KTXH%20th%C3%A1ng%20-%20Kh%C3%A1ch%20qu%E1%BB%91c%20t%E1%BA%BF?rs:embed=true"
        if let urlComponents = NSURLComponents(string: urlStr) {
            urlComponents.user = "gemteam"
            urlComponents.password = "Edso@xsm1x0#<$"
            
            let requestObj = URLRequest(url: urlComponents.url!)
            webView.loadRequest(requestObj)
        }
    }
}
