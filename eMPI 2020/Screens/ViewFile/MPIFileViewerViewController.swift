//
//  MPIFileViewerViewController.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/16/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIFileViewerViewController: TBaseViewController {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDemoFile()
    }
    
    @IBAction func btnClosePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadDemoFile() {
//        if let pdfURL = Bundle.main.url(forResource: "demo", withExtension: "pdf", subdirectory: nil, localization: nil),
//            let data = NSData(contentsOf: pdfURL),
//            let baseURL = pdfURL.lastPathComponent  {
//            webView.load(data, mimeType: "application/pdf", textEncodingName: "", baseURL: baseURL)
////            let webView = UIWebView(frame: CGRectMake(20,20,self.view.frame.size.width-40,self.view.frame.size.height-40))
////            webView.loadData(data, MIMEType: "application/pdf", textEncodingName:"", baseURL: baseURL)
////            self.view.addSubview(webView)
//        }
        
        if let pdfUrl = Bundle.main.url(forResource: "demo", withExtension: "pdf", subdirectory: nil, localization: nil) {

            do {
                let data = try Data(contentsOf: pdfUrl)
                webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfUrl.deletingLastPathComponent())
                print("pdf file loading...")
            }
            catch {
                print("failed to open pdf")
            }
            return
        }
    }
}
