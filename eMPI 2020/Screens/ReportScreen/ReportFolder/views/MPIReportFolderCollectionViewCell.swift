//
//  MPIReportFolderCollectionViewCell.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIReportFolderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: ThemeLabelSubTitle!
    @IBOutlet weak var imvType: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func billData(_ data: MPIReportCategoryModel) {
        lblTitle.text = data.name
        imvType.image = UIImage(named: data.type == .folder ? "ic_report_folder" : "ic_report_file")
    }
}
