//
//  MPIReportFolderViewController.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/11/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIReportFolderViewController: UIViewController {

    @IBOutlet weak var colView: UICollectionView!
    private var data: [MPIReportCategoryModel] = [MPIReportCategoryModel("Số liệu đầu tư nước ngoài", MPIReportType.folder),
                                                  MPIReportCategoryModel("Số liệu đăng ký kinh doanh", MPIReportType.folder),
                                                  MPIReportCategoryModel("Kinh tế xã hội - Tháng", MPIReportType.folder),
                                                  MPIReportCategoryModel("Kinh tế xã hội - Quý", MPIReportType.folder),
                                                  MPIReportCategoryModel("Kinh tế xã hội - Năm", MPIReportType.folder),
                                                  MPIReportCategoryModel("Đấu thầu", MPIReportType.folder),
                                                  MPIReportCategoryModel("Tổng hợp tình hình xử lý văn bản", MPIReportType.folder),
                                                  MPIReportCategoryModel("Tổng hợp về xử lý hồ sơ TTHC", MPIReportType.folder),
                                                  MPIReportCategoryModel("Các số liệu chỉ đạo điều hành của Bộ", MPIReportType.folder)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colView.register(MPIReportFolderCollectionViewCell.nib(), forCellWithReuseIdentifier: "cell")
        colView.alwaysBounceVertical = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension MPIReportFolderViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MPIReportFolderCollectionViewCell
        let item = data[indexPath.row]
        cell.billData(item)
        return cell
    }
}

extension MPIReportFolderViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberItemInLine: CGFloat = 5.0
        let paddingItem: CGFloat = 10.0
        let padding: CGFloat = paddingItem*numberItemInLine
        let width = (collectionView.frame.width - 32.0 - padding)/numberItemInLine
        return CGSize(width: width, height: 180.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20.0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

extension MPIReportFolderViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DLog("didSelectItemAt: \(indexPath.row)")
        let vc = MPIReportFolderDetailViewController()
        vc.folder = data[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
