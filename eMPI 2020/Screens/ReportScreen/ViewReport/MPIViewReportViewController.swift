//
//  MPIViewReportViewController.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIViewReportViewController: TBaseViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var imbBack: UIImageView!
    @IBOutlet weak var lblTitle: ThemeLabelTitle!
    var report: MPIReportCategoryModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = report?.name
        webView.backgroundColor = theme.tableBackgroundColor
        webView.scalesPageToFit = true
        loadReport()
        
        weak var weakSelf = self
        imbBack.addAction {
            weakSelf?.dismiss(animated: true, completion: nil)
        }
    }
    
    func loadReport() {
        let urlStr = "http://pbi.mpi.gov.vn/reports/powerbi/UAT/KTXH%20th%C3%A1ng%20-%20Kh%C3%A1ch%20qu%E1%BB%91c%20t%E1%BA%BF?rs:embed=true"
        if let urlComponents = NSURLComponents(string: urlStr) {
            urlComponents.user = "gemteam"
            urlComponents.password = "Edso@xsm1x0#<$"
            
            let requestObj = URLRequest(url: urlComponents.url!)
            webView.loadRequest(requestObj)
        }
    }
}
