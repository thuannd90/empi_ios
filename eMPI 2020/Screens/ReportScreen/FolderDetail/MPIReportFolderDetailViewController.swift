//
//  MPIReportFolderDetailViewController.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit
import Actions

class MPIReportFolderDetailViewController: UIViewController {

    @IBOutlet weak var imvBack: UIImageView!
    @IBOutlet weak var lblTitle: ThemeLabelTitle!
    @IBOutlet weak var colView: UICollectionView!
    var folder: MPIReportCategoryModel? = nil
    
    private var data: [MPIReportCategoryModel] = [MPIReportCategoryModel("Đầu tư nước ngoài (M)", .report),
                                                  MPIReportCategoryModel("Đầu tư nước ngoài (R)", .report),
                                                  MPIReportCategoryModel("Đăng ký doanh nghiệp (R)", .report),
                                                  MPIReportCategoryModel("Đăng ký doanh nghiệp (M)", .report),
                                                  MPIReportCategoryModel("Đấu thầu qua mạng", .report),
                                                  MPIReportCategoryModel("Nhiệm vụ CP, TTCP giao", .report),
                                                  MPIReportCategoryModel("Đầu tư công", .report),
                                                  MPIReportCategoryModel("Giám sát đầu tư", .report),
                                                  MPIReportCategoryModel("Quy hoạch", .report),
                                                  MPIReportCategoryModel("Hợp tác xã", .report),
                                                  MPIReportCategoryModel("Hợp tác xã - Cục HTX", .report),
                                                  MPIReportCategoryModel("KTXH tháng - Công nghiệp", .report),
                                                  MPIReportCategoryModel("KTXH tháng - SX Nông nghiệp", .report),
                                                  MPIReportCategoryModel("KTXH tháng - Khách quốc tế", .report),
                                                  MPIReportCategoryModel("KTXH tháng - Bán lẻ, tiêu dùng", .report),
                                                  MPIReportCategoryModel("KTXH tháng - Chỉ số giá", .report),
                                                  MPIReportCategoryModel("KTXH tháng - Vốn đầu tư NSNN", .report),
                                                  MPIReportCategoryModel("KTXH tháng - Xuất, nhập khẩu hàng hoá", .report)]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        weak var weakSelf = self
        imvBack.addAction {
            weakSelf?.navigationController?.popViewController(animated: true)
        }
        
        colView.register(MPIReportFolderCollectionViewCell.nib(), forCellWithReuseIdentifier: "cell")
        colView.alwaysBounceVertical = true
        lblTitle.text = folder?.name
    }
}

extension MPIReportFolderDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MPIReportFolderCollectionViewCell
        let item = data[indexPath.row]
        cell.billData(item)
        return cell
    }
}

extension MPIReportFolderDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberItemInLine: CGFloat = 5.0
        let paddingItem: CGFloat = 10.0
        let padding: CGFloat = paddingItem*numberItemInLine
        let width = (collectionView.frame.width - 32.0 - padding)/numberItemInLine
        return CGSize(width: width, height: 180.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20.0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

extension MPIReportFolderDetailViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DLog("didSelectItemAt: \(indexPath.row)")
        let vc = MPIViewReportViewController()
        vc.report = data[indexPath.row]
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
