//
//  MPIMeetingDayViewController.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit
import FSCalendar

class MPIMeetingDayViewController: TBaseViewController {

    @IBOutlet weak var calendarView: TCalendarView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendarContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var viewChangeSize: ThemeViewBackgroundTable!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableDetail: MPIMeetingDetailTableView!
    @IBOutlet weak var lblTitle: ThemeLabelTitle!
    
    var data: [MPIMeetingModel] = [MPIMeetingModel("15/08",
                                                   "08:30",
                                                   "Họp trực tiếp",
                                                   "Bộ trưởng Nguyễn Chí Dũng chủ trì họp \"Thảo luận các giải pháp, chính sách tiếp tục tháo gỡ khó khăn cho sản xuất kinh doanh, bảo đảm ổn định"),
    MPIMeetingModel("15/08",
    "14:00",
    "Họp trực tiếp",
    "Thứ trưởng Nguyễn Văn Trung chủ trì hội nghị thực hiện quy trình giới thiệu nhân sự lần đầu tham gia cấp uỷ, UBKTĐUCQ nhiệm kỳ 2021-2025 - Hội trường A (Hội trường lớn)"),
    
    MPIMeetingModel("18/08",
    "14:00",
    "Họp trực tiếp",
    "Kiểm toán nhà nước làm việc với một số đơn vị của Bộ (cả ngày) - Phòng 235 Nhà A"),
    
    MPIMeetingModel("17/08",
    "08:00",
    "Họp trực tiếp",
    "Vụ TĐKT & TT họp với cục ĐTNN về tôn vih doanh nghiệp ĐTNN tiêu biểu - Hội trường - Tầng 4 (P401)"),
    
    MPIMeetingModel("17/08",
    "08:00",
    "Họp trực tiếp",
    "Cục Quản lý đầu thầu họp  giao bản Cục - Hội trường A (Hội trường lớn)"),
    
    MPIMeetingModel("15/08",
    "14:00",
    "Họp trực tiếp",
    "Thứ trưởng Nguyễn Văn Trung chủ trì hội nghị thực hiện quy trình giới thiệu nhân sự lần đầu tham gia cấp uỷ, UBKTĐUCQ nhiệm kỳ 2021-2025 - Hội trường A (Hội trường lớn)"),
    
    MPIMeetingModel("18/08",
    "14:00",
    "Họp trực tiếp",
    "Kiểm toán nhà nước làm việc với một số đơn vị của Bộ (cả ngày) - Phòng 235 Nhà A"),
    
    MPIMeetingModel("17/08",
    "08:00",
    "Họp trực tiếp",
    "Vụ TĐKT & TT họp với cục ĐTNN về tôn vih doanh nghiệp ĐTNN tiêu biểu - Hội trường - Tầng 4 (P401)"),
    
    MPIMeetingModel("17/08",
    "08:00",
    "Họp trực tiếp",
    "Cục Quản lý đầu thầu họp  giao bản Cục - Hội trường A (Hội trường lớn)")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        let item = data[0]
        tableDetail.showDetail(item)
        lblTitle.text = item.title
        
        tableDetail.rootVC = self
    }
    
    func setUpView() {
        self.calendarView.delegate = self
        self.calendarView.scopeGesture.delegate = self
        self.calendarView.scopeTableViewGesture.delegate = self
        self.calendarView.scope = .week
        topView.addGestureRecognizer(self.calendarView.scopeGesture)
        tableView.separatorColor = theme.separatorColor
    }
}

extension MPIMeetingDayViewController: TCalendarViewDelegate {
    func calendar(_ calendar: TCalendarView, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarContainerHeight.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: TCalendarView, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
    }
}

extension MPIMeetingDayViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.calendarView.scopeGesture {
            return true
        }

        let shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top
        if shouldBegin {
            let velocity = self.calendarView.scopeTableViewGesture.velocity(in: self.tableView)
            switch self.calendarView.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            @unknown default:
                return true
            }
        }
        return shouldBegin
    }
}

extension MPIMeetingDayViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MPIMeetingTableViewCell.dequeCellWithTable(tableView)
        cell.selectionStyle = .none
        cell.billData(data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        weak var weakself = self
        DispatchQueue.main.async {
            if let strongSelf = weakself {
                let item = strongSelf.data[indexPath.row]
                strongSelf.tableDetail.showDetail(item)
                strongSelf.lblTitle.text = item.title
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
