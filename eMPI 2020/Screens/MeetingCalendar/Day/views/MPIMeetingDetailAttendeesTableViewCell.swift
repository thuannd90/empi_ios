//
//  MPIMeetingDetailAttendeesTableViewCell.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/16/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIMeetingDetailAttendeesTableViewCell: UITableViewCell {

    @IBOutlet weak var imvIcon: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scrollView.contentSize = CGSize(width: 640.0, height: 80.0)
        scrollView.alwaysBounceHorizontal = true
        scrollView.isScrollEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func billData(_ title: String, _ image: String) {
        self.selectionStyle = .none
        self.imvIcon?.image = UIImage(named: image)
        
       
    }
}
