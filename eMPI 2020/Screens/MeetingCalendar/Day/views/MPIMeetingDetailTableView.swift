//
//  MPIMeetingDetailTableView.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIMeetingDetailTableView: UITableView {

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }
    
    func setUpView() {
        self.dataSource = self
        self.delegate = self
    }
    
    var rootVC: UIViewController? = nil
    
    private var data: [MeetingData] = [
        MeetingData(icon: "ic_clock",
                    title: "",
                    content: "10/08/2020 09:30 - 10:00",
                    dataType: .infor),
        MeetingData(icon: "ic_desc",
                    title: "",
                    content: "Kiểm toán Nhà nước làm việc với một số đơn vị của Bộ (Cả ngày) - Phòng 235 Nhà A",
                    dataType: .infor),
        MeetingData(icon: "ic_may_chieu",
                    title: "",
                    content: "Không sử dụng máy chiếu",
                    dataType: .infor),
        MeetingData(icon: "ic_user_group",
                    title: "",
                    content: "Tổng số người tham dự: 10\nKhách quốc tế: 2\nKhách ở cơ quan bên ngoài: 2",
                    dataType: .attendees),
        MeetingData(icon: "ic_phone_call",
                    title: "Thông tin liên hệ",
                    content: "Họ tên:\nSĐT cơ quan:\nSĐT di động:",
                    dataType: .infor),
        MeetingData(icon: "ic_att",
                    title: "Thông tin đính kèm",
                    content: "",
                    dataType: .att)]

    func showDetail(_ detail: MPIMeetingModel) {
        let newData: [MeetingData] = [
        MeetingData(icon: "ic_clock",
                    title: "",
                    content: "10/08/2020 09:30 - 10:00",
                    dataType: .infor),
        
        MeetingData(icon: "ic_desc",
                    title: "",
                    content: detail.content,
                    dataType: .infor),
        
        MeetingData(icon: "ic_may_chieu",
                    title: "",
                    content: "Không sử dụng máy chiếu",
                    dataType: .infor),
        
        MeetingData(icon: "ic_user_group",
                    title: "",
                    content: "Tổng số người tham dự: 10\nKhách quốc tế: 2\nKhách ở cơ quan bên ngoài: 2",
                    dataType: .attendees),
        
        MeetingData(icon: "ic_phone_call",
                    title: "Thông tin liên hệ",
                    content: "Họ tên:\nSĐT cơ quan:\nSĐT di động:",
                    dataType: .infor),
        
        MeetingData(icon: "ic_att",
                    title: "Thông tin đính kèm",
                    content: "",
                    dataType: .att)]
        
        data = newData
        self.reloadData()
    }
}

extension MPIMeetingDetailTableView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data[indexPath.row]
        
        if item.dataType == .attendees {
            let cell = MPIMeetingDetailAttendeesTableViewCell.dequeCellWithTable(tableView)
            cell.billData(item.title, item.icon)
            return cell
        }
        
        if item.dataType == .att {
            let cell = MPIMeetingDetailAttTableViewCell.dequeCellWithTable(tableView)
            cell.billData(item.title, item.icon)
            
            weak var weakSelf = self
            cell.doc1.addAction {
                weakSelf?.doShowDoc()
            }
            
            cell.doc2.addAction {
                weakSelf?.doShowDoc()
            }
            
            return cell
        }
        
        let cell = MPIMeetingDetailTableViewCell.dequeCellWithTable(tableView)
        cell.billData(item.title, item.content, item.icon)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//    }
//    
    func doShowDoc() {
        let vc = MPIFileViewerViewController()
        rootVC?.present(vc, animated: true, completion: nil)
    }
}

private struct MeetingData {
    var icon: String = ""
    var title: String = ""
    var content: String = ""
    var dataType: MeetingDataType = MeetingDataType.infor
}

private enum MeetingDataType {
    case att
    case infor
    case attendees
}
