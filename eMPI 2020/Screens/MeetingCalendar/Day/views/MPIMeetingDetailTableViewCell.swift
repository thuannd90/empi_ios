//
//  MPIMeetingDetailTableViewCell.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIMeetingDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imvIcon: UIImageView!
    @IBOutlet weak var lblTitle: ThemeLabelTitle!
    @IBOutlet weak var lblContent: ThemeLabelSubTitle!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func billData(_ title: String, _ content: String, _ image: String) {
        self.selectionStyle             = .none
        self.lblTitle.text              = title
        self.lblContent.text            = content
        self.lblTitle.isHidden          = title.isEmpty
        self.lblContent.isHidden        = content.isEmpty
        self.imvIcon?.image             = UIImage(named: image)
    }
    
//    func generateAttStr(_ content: String, _ font: UIFont)-> NSAttributedString {
//        let attributedString = NSMutableAttributedString(string: content)
//
//        // *** Create instance of `NSMutableParagraphStyle`
//        let paragraphStyle = NSMutableParagraphStyle()
//
//        // *** set LineSpacing property in points ***
//        paragraphStyle.lineSpacing = 4.0
//
//        // *** Apply attribute to string ***
//        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
//                                      value:paragraphStyle,
//                                      range:NSMakeRange(0, attributedString.length))
//
////        attributedString.addAttribute(NSAttributedString.Key.font,
////                                      value:font,
////                                      range:NSMakeRange(0, attributedString.length))
//
//
//        // *** Set Attributed String to your label ***
//        return attributedString
//    }
}
