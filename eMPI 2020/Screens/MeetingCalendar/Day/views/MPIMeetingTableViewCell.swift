//
//  MPIMeetingTableViewCell.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIMeetingTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDay: ThemeLabelTitle!
    @IBOutlet weak var lblTime: ThemeLabelTitle!
    @IBOutlet weak var lblTitle: ThemeLabelTitle!
    @IBOutlet weak var lblContent: ThemeLabelSubTitle!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func billData(_ data: MPIMeetingModel) {
        lblDay.text     = data.day
        lblTime.text    = data.time
        lblTitle.text   = data.title
        lblContent.text = data.content
    }
}
