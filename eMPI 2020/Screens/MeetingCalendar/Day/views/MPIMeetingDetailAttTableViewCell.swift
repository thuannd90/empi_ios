//
//  MPIMeetingDetailTableViewCell.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIMeetingDetailAttTableViewCell: UITableViewCell {

    @IBOutlet weak var imvIcon: UIImageView!
    @IBOutlet weak var lblTitle: ThemeLabelTitle!
    @IBOutlet weak var doc1: UIView!
    @IBOutlet weak var doc2: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func billData(_ title: String, _ imageName: String) {
        self.selectionStyle         = .none
        self.lblTitle.text          = title
        self.lblTitle.isHidden      = title.isEmpty
        self.imvIcon?.image         = UIImage(named: imageName)
    }
    
    func showDoc1() {
        
    }
    
    func showDoc2() {
        
    }
}
