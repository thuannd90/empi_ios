//
//  MPIMeetingCalendarViewController.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIMeetingCalendarViewController: TBaseViewController {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var segment: ThemeSegmentView!
    private var tabbarVC: UITabBarController? = nil
    
    private lazy var dayVC: MPIMeetingDayViewController = {
       return MPIMeetingDayViewController()
    }()
    
    private lazy var monthVC: MPIMeetingMonthViewController = {
        return MPIMeetingMonthViewController()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK:- IBACTIONS
    @IBAction func segmentChangeValue(_ sender: Any) {
        let index = segment.selectedSegmentIndex
        tabbarVC?.selectedViewController = index == 0 ? dayVC : monthVC
    }
    
    //MARK:- METHODS
    func setupTabbar() {
        let _tabbarVC = UITabBarController()
        _tabbarVC.setViewControllers([dayVC, monthVC], animated: false)
        _tabbarVC.view.backgroundColor = .clear
        _tabbarVC.tabBar.isHidden = true
        
        bottomView.addSubview(_tabbarVC.view)
        _tabbarVC.view.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        self.addChild(_tabbarVC)
        tabbarVC = _tabbarVC
    }
    
}
