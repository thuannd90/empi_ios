//
//  MPIMeetingModel.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/16/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIMeetingModel: NSObject {
    var day: String     = ""
    var time: String    = ""
    var title: String   = ""
    var content: String = ""
    
    init(_ _day: String, _ _time: String, _ _title: String, _ _content: String) {
        day     = _day
        time    = _time
        title   = _title
        content = _content
    }
}
