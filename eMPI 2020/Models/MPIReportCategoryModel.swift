//
//  MPIReportCategoryModel.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

enum MPIReportType {
    case folder
    case report
}

class MPIReportCategoryModel: NSObject {
    var name: String = ""
    var type: MPIReportType = MPIReportType.folder
    
    init(_ _name: String, _ _type: MPIReportType) {
        name = _name
        type = _type
    }
}
