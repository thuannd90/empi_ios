//
//  MPINotificationModel.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/16/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPINotificationModel: NSObject {
    var title: String = ""
    var content: String = ""
    var time: String = ""
    
    init(_ _title: String, _ _content: String, _ _time: String) {
        title = _title
        content = _content
        time = _time
    }
}
