//
//  MPIAppModel.swift
//  eMPI 2020
//
//  Created by iMac on 8/12/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class MPIAppModel: NSObject {
    var name: String = ""
    var desc: String = ""
    var imageName: String = ""
    
    init(_ _name: String, _ _desc: String, _ _imageName: String) {
        name = _name
        desc = _desc
        imageName = _imageName
    }
}
