//
//  UIImageExtension.swift
//  Utils
//
//  Created by iMac on 4/27/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

extension UIImage {
    func scaleImage(size: CGSize) -> UIImage? {
        let rect: CGRect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        let cgImage: CGImage = self.cgImage!.cropping(to: rect)!
        return UIImage(cgImage: cgImage,
                       scale: self.size.width/size.width,
                       orientation: self.imageOrientation)
    }
    
    func scaleImageToWidth(_ scale: CGFloat) -> UIImage? {
        let scaleValue = scale/self.size.width
        let newSize = CGSize(width: scaleValue*self.size.width, height: scaleValue*self.size.height)
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension UIImageView {
    func kSetImageTinColor(_ _tintColor: UIColor) {
        let tempImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = tempImage
        self.tintColor = _tintColor
    }
}
