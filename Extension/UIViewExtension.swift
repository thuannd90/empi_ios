//
//  UIViewExtension.swift
//  Utils
//
//  Created by Macintosh on 9/19/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func initWithDefaultNib() -> Self {
        return initWithNibTemplate()
    }
    
    class func initWithNibName(_ nibName: String) -> Self {
        return initWithNibNameTemplate(nibName)
    }
    
    private class func initWithNibNameTemplate<T>(_ nibName: String) -> T {
        let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first
        return view as! T
    }
    
    private class func initWithNibTemplate<T>() -> T {
        let nibName = String(describing: self)
        let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first
        return view as! T
    }
    
    class func nib() -> UINib {
        let nibName = String(describing: self)
        return UINib(nibName: nibName, bundle: nil)
    }
    
    var theme: AppThemeInterface {
        return AppTheme.themeDefault
    }
}
