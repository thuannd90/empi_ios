//
//  DateExtension.swift
//  Utils
//
//  Created by Macintosh on 9/20/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import Foundation

extension Date {
    func toString()-> String {
        return self.toStringWithFormat(AppDateFormat.defaultFormat.format())
    }
    
    func toStringWithFormat(_ format: String)-> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        
        let locale = TLocalizationStringHelper.currentLanguage.locale
        formatter.locale = Locale(identifier: locale)
        return formatter.string(from: self).capitalized
    }
    
    func nextMonth()-> Date {
        let cal = NSCalendar.current
        if let newDate = cal.date(byAdding: .month, value: 1, to: self) {
            return newDate
        }
        return self
    }
    
    func nextDay()-> Date {
        let cal = NSCalendar.current
        if let newDate = cal.date(byAdding: .day, value: 1, to: self) {
            return newDate
        }
        return self
    }
    
    func previousMonth()-> Date {
        let cal = NSCalendar.current
        if let newDate = cal.date(byAdding: .month, value: -1, to: self) {
            return newDate
        }
        return self
    }
    
    func previousYear()-> Date {
        let cal = NSCalendar.current
        if let newDate = cal.date(byAdding: .year, value: -1, to: self) {
            return newDate
        }
        return self
    }
    
    func nextYear()-> Date {
        let cal = NSCalendar.current
        if let newDate = cal.date(byAdding: .year, value: 1, to: self) {
            return newDate
        }
        return self
    }
    
    func getDay()-> String {
        return self.toStringWithFormat("dd")
    }
    
    func getMonth()-> String {
        return self.toStringWithFormat("MM")
    }
    
    func getYear()-> String {
        return self.toStringWithFormat("yyyy")
    }
    
    func getDayInWeek()-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        
        let locale = TLocalizationStringHelper.currentLanguage.locale
        dateFormatter.locale = Locale(identifier: locale)
        
        return dateFormatter.string(from: self).capitalized
    }
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    
    func isThisMonth()-> Bool {
        let date = Date()
        if self.getYear() == date.getYear(), self.getMonth() == date.getMonth() {
            return true
        }
        return false
    }
    
    var keyMonthYear: String {
        return "\(self.getMonth())-\(self.getYear())"
    }
    
    var keyDayMonthYear: String {
        return "\(self.getDay())-\(self.getMonth())-\(self.getYear())"
    }
    
    var isSunday: Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let str = dateFormatter.string(from: self).uppercased()
        return str == "SUN"
    }
    
    var isWeekend: Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let str = dateFormatter.string(from: self).uppercased()
        return str == "SUN" || str == "SAT"
    }
    
    var isInYesterday: Bool { Calendar.current.isDateInYesterday(self) }
    var isInToday:     Bool { Calendar.current.isDateInToday(self) }
    var isInTomorrow:  Bool { Calendar.current.isDateInTomorrow(self) }

    var isInTheFuture: Bool { self > Date() }
    var isInThePast:   Bool { self < Date() }
    
    func isEqual(to date: Date,
                 toGranularity component: Calendar.Component,
                 in calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: component)
    }
    
    func isInSameYear (date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
    func isInSameMonth(date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
    func isInSameDay  (date: Date) -> Bool { isEqual(to: date, toGranularity: .day) }
    func isInSameWeek (date: Date) -> Bool { isEqual(to: date, toGranularity: .weekOfYear) }

    var isInThisYear:  Bool { isInSameYear(date: Date()) }
    var isInThisMonth: Bool { isInSameMonth(date: Date()) }
    var isInThisWeek:  Bool { isInSameWeek(date: Date()) }
    
    static func getDate(_ day: Int, _ month: Int, _ year: Int)-> Date? {
        let str = "\(day)/\(month)/\(year)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d/M/yyyy"
        
        let locale = TLocalizationStringHelper.currentLanguage.locale
        dateFormatter.locale = Locale(identifier: locale) // set locale to reliable US_POSIX
        return dateFormatter.date(from: str)
    }
    
    static func getDateValue(_ day: Int, _ month: Int, _ year: Int)-> Date {
        let str = "\(day)/\(month)/\(year)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d/M/yyyy"
        
        let locale = TLocalizationStringHelper.currentLanguage.locale
        dateFormatter.locale = Locale(identifier: locale) // set locale to reliable US_POSIX
        return dateFormatter.date(from: str)!
    }
    
    var getSolarDay: Int {
        let calendar = Calendar.current
        let month = calendar.component(.day, from: self)
        return month
    }
    
    var getSolarMonth: Int {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self)
        return month
    }
    
    var getSolarYear: Int {
        let calendar = Calendar.current
        let month = calendar.component(.year, from: self)
        return month
    }
    
    var getHour: Int {
        let calendar = Calendar.current
        let month = calendar.component(.hour, from: self)
        return month
    }
    
    var getMinute: Int {
        let calendar = Calendar.current
        let month = calendar.component(.minute, from: self)
        return month
    }
    
    var getDayInWeekStr: String {
        let day = self.getDayIndexOfWeek()
        if day == 1 {
            return "Chủ nhật"
        }
           
        if day == 2 {
            return "Thứ hai"
        }
           
        if day == 3 {
            return "Thứ ba"
        }
           
        if day == 4 {
            return "Thứ tư"
        }
           
        if day == 5 {
            return "Thứ năm"
        }
           
        if day == 6 {
            return "Thứ sáu"
        }
           
        if day == 7 {
            return "Thứ bảy"
        }
        return "Unknow"
    }
    
    func getDayIndexOfWeek() -> Int {
        let calendar = Calendar.current
        let day = calendar.component(.weekday, from: self)
        return day
    }
    
    var getSolarMonthStr: String {
           return "Tháng \(self.toStringWithFormat("MM"))"
       }
}
