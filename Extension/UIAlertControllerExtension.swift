//
//  UIAlertControllerExtension.swift
//  Utils
//
//  Created by iMac on 3/6/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

extension UIAlertController {
    class func controller(title: String?, message: String?) -> UIAlertController {
        let interface = UIDevice.current.userInterfaceIdiom
        let preferredStyle: UIAlertController.Style = interface == .pad ? .alert : .actionSheet
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: preferredStyle)
        
        if #available(iOS 13.0, *) {
            if AppTheme.themeDefault.isStatusBarLightStyle {
                alert.overrideUserInterfaceStyle = .dark
            } else {
                alert.overrideUserInterfaceStyle = .light
            }
        }
        return alert
    }
    
    class func controller(title: String?, message: String?, style: UIAlertController.Style) -> UIAlertController {
//        _ = UIDevice.current.userInterfaceIdiom
    
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: style)
        
        if #available(iOS 13.0, *) {
            if AppTheme.themeDefault.isStatusBarLightStyle {
                alert.overrideUserInterfaceStyle = .dark
            } else {
                alert.overrideUserInterfaceStyle = .light
            }
        }
        return alert
    }
}
