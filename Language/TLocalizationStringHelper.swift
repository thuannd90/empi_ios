//
//  TLocalizationStringHelper.swift
//  Utils
//
//  Created by Macintosh on 9/19/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import Foundation
import Localize

struct LanguageTypeProperty {
    var locale: String
    var title: String
    var desc: String
    var icon: String
    
    var searchString: String {
        return (title + desc).searchString
    }
}

enum LanguageType: String {
    case vietnam = "vi"
    
    var locale: String {
        return LanguageType.mapProperties[self]?.locale ?? ""
    }
    
    var title: String {
        return LanguageType.mapProperties[self]?.title ?? ""
    }
    
    var desc: String {
        return LanguageType.mapProperties[self]?.desc ?? ""
    }
    
    var icon: String {
        return LanguageType.mapProperties[self]?.icon ?? ""
    }
    
    static func typeWithStr(_ str: String) -> LanguageType {
        return map[str] ?? LanguageType.vietnam
    }
    
    static let map: [String: LanguageType] = [LanguageType.vietnam.rawValue: LanguageType.vietnam]
    
    static var allLanguages: [LanguageType] {
        let list: [LanguageType] = [.vietnam]
        return list
    }
    
    static let mapProperties: [LanguageType: LanguageTypeProperty] =
        [LanguageType.vietnam: LanguageTypeProperty(locale: "vi",
                                                    title: "Vietnamese",
                                                    desc: "Tiếng Việt",
                                                    icon: "VN.png")]
}

class TLocalizationStringHelper: NSObject {
    
    static func setUpDefaultLanguage () {
        let localize = Localize.shared
        localize.update(provider: .json)
        // Set your file name
        localize.update(fileName: "lang")
        
        let language = LanguageType.vietnam.rawValue
        localize.update(defaultLanguage: language)
        localize.update(language: language)
    }
    
    static var currentLanguage: LanguageType {
        return LanguageType.vietnam
    }
    
    static func updateLanguage(_ type: LanguageType) {
//        if TFAppDataManager.shareInstance.currentLanguage == type {
//            return
//        }
//
//        Localize.shared.update(language: type.rawValue)
//        TFAppDataManager.shareInstance.currentLanguage = type
//
//        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
//            appDelegate.doShowHomeScreen(true)
//        }
    }
    
    class var _today: String    { return "_login".localize() }
    class var _note: String    { return "_note".localize() }
}
