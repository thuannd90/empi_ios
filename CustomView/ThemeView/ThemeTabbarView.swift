//
//  ThemeTabbarView.swift
//  Utils
//
//  Created by Macintosh on 5/1/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ThemeTabbarView: UITabBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTheme()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpTheme()
    }
    
    private func setUpTheme() {
        self.isTranslucent   = false
        self.isOpaque        = true
        self.barTintColor    = theme.backgroundColor
        self.tintColor       = theme.navigationItemTintColor
        
        if #available(iOS 10.0, *) {
            self.unselectedItemTintColor = theme.tabbarItemUnselectedColor
        }
    }
}
