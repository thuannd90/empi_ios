//
//  AppThemeInterface.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

protocol AppThemeInterface: NSObjectProtocol {
    var key: String { get }
    var navigationBackgroundColor: UIColor { get }
    var navigationItemTintColor: UIColor { get }
    var backgroundColor: UIColor { get }
    var titleColor: UIColor { get }
    var subTitleColor: UIColor { get }
    
    var tableBackgroundColor: UIColor { get }
    var tableBackgroundColor2: UIColor { get }
    var tableCellBackgroundColor: UIColor { get }
    var tableCellTitleColor: UIColor { get }
    var tableCellSubTitleColor: UIColor { get }
    
    var calendarDayBackgroundInMonth: UIColor { get }
    var calendarDayBackgroundNotInMonth: UIColor { get }
    var calendarBackgroundToday: UIColor { get }
    var searchViewBackgroundColor: UIColor { get }
    var searchViewTextColor: UIColor { get }
    var searchViewPlaceholderColor: UIColor { get }
    var separatorColor: UIColor { get }
    
    var segmentBackgroundColor: UIColor { get }
    var segmentTextColor: UIColor { get }
    var segmentSelectedBackgroundColor: UIColor { get }
    var segmentSelectedTextColor: UIColor { get }
    
    var progressViewBackgroundColor: UIColor { get }
    var progressViewTrackTintColor: UIColor { get }
    
    var keyboardSeparatorColor: UIColor { get }
    var tableViewSeparatorColor: UIColor { get }
    var isStatusBarLightStyle: Bool { get }
    var tabbarItemUnselectedColor: UIColor { get }
    
    var thumbColor: UIColor { get }
    var hightlightColor: UIColor { get }
    var tableViewAccessoryColor: UIColor { get }
}

class AppTheme: NSObject {
    fileprivate static let key = "AppTheme"
    fileprivate static let userDefault = UserDefaults.standard
    
    static let list: [AppThemeInterface] = [AppThemeNormal(),
                                            AppThemeDarkBlue(),
                                            AppThemeDark(),
                                            AppThemeLightGray()]
    
    static let map: [String: AppThemeInterface] = ["AppThemeNormal": AppThemeNormal(),
                                                   "AppThemeDarkBlue" : AppThemeDarkBlue(),
                                                   "AppThemeDark" : AppThemeDark(),
                                                   "AppThemeLightGray": AppThemeLightGray()]
    
    static var isSetupTheme: Bool {
        let userDefault = UserDefaults.standard
        guard let keyValue = userDefault.value(forKey: key) as? String else {
            return false
        }
        return map[keyValue] != nil ? true : false
    }
    
    static var mThemeDefault: AppThemeInterface = {
        let userDefault = UserDefaults.standard
        guard let keyValue = userDefault.value(forKey: key) as? String else {
            return AppThemeDarkBlue()
        }
        
        return map[keyValue] ?? AppThemeDarkBlue()
    }()
    
    static var themeDefault: AppThemeInterface {
        return mThemeDefault
    }
    
    static func setTheme(_ theme: AppThemeInterface) {
        if themeDefault.key == theme.key {
            return
        }
        
        mThemeDefault = theme
        userDefault.setValue(theme.key, forKey: key)
        userDefault.synchronize()
    }
}

private class AppThemeNormal: NSObject, AppThemeInterface {
    var key: String                                 = "AppThemeNormal"
    var navigationBackgroundColor: UIColor          = UIColor.white
    var navigationItemTintColor: UIColor            = TAppColor.greenColor
    var backgroundColor: UIColor                    = UIColor.white
    var titleColor: UIColor                         = UIColor.black
    var subTitleColor: UIColor                      = UIColor.darkGray
    
    var tableBackgroundColor: UIColor               = UIColor.groupTableViewBackground
    var tableBackgroundColor2: UIColor              = UIColor.groupTableViewBackground
    var tableCellBackgroundColor: UIColor           = UIColor.white
    var tableCellTitleColor: UIColor                = UIColor.black
    var tableCellSubTitleColor: UIColor             = UIColor.darkGray
    
    var calendarDayBackgroundInMonth: UIColor       = UIColor.white
    var calendarDayBackgroundNotInMonth: UIColor    = UIColor.groupTableViewBackground
    var calendarBackgroundToday: UIColor            = UIColor.systemGreen.withAlphaComponent(0.2)
    
    var searchViewBackgroundColor: UIColor          = UIColor.groupTableViewBackground
    var searchViewTextColor: UIColor                = UIColor.black
    var searchViewPlaceholderColor: UIColor         = UIColor.lightGray
    var separatorColor: UIColor                     = UIColor(red: 213/255.0, green: 213/255.0, blue: 213/255.0, alpha: 1.0)
    
    var segmentBackgroundColor: UIColor             = UIColor.groupTableViewBackground
    var segmentTextColor: UIColor                   = UIColor.darkGray
    var segmentSelectedBackgroundColor: UIColor     = UIColor.white
    var segmentSelectedTextColor: UIColor           = UIColor.black
    
    var progressViewBackgroundColor: UIColor        = UIColor.hexColor("#f0f0f0", 1.0)
    var progressViewTrackTintColor: UIColor         = TAppColor.greenColor
    
    var keyboardSeparatorColor: UIColor             = UIColor.groupTableViewBackground
    var tableViewSeparatorColor: UIColor            = UIColor(red: 224/255.0, green: 224/255.0, blue: 224/255.0, alpha: 1.0)
    var isStatusBarLightStyle: Bool                 = false
    var tabbarItemUnselectedColor: UIColor          = UIColor.lightGray
    
    var thumbColor: UIColor                         = UIColor.white
    var hightlightColor: UIColor                    = UIColor(red: 235.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
    var tableViewAccessoryColor: UIColor            = UIColor.lightGray
}

private class AppThemeDark: NSObject, AppThemeInterface {
    internal static let mainColor = UIColor(red: 28/255.0, green: 28/255.0, blue: 30/255.0, alpha: 1.0)
    
    var key: String                                 = "AppThemeDark"
    var navigationBackgroundColor: UIColor          = mainColor
    var navigationItemTintColor: UIColor            = TAppColor.greenColor
    var backgroundColor: UIColor                    = mainColor
    var titleColor: UIColor                         = UIColor.white
    var subTitleColor: UIColor                      = UIColor.lightGray
    
    var tableBackgroundColor: UIColor               = UIColor.black
    var tableBackgroundColor2: UIColor              = UIColor.black
    var tableCellBackgroundColor: UIColor           = mainColor
    var tableCellTitleColor: UIColor                = UIColor.white
    var tableCellSubTitleColor: UIColor             = UIColor.lightGray
    
    var calendarDayBackgroundInMonth: UIColor       = mainColor
    var calendarDayBackgroundNotInMonth: UIColor    = UIColor.black
    var calendarBackgroundToday: UIColor            = UIColor.white.withAlphaComponent(0.2)
    var searchViewBackgroundColor: UIColor          = UIColor.darkGray
    var searchViewTextColor: UIColor                = UIColor.white
    var searchViewPlaceholderColor: UIColor         = UIColor.lightGray
    var separatorColor: UIColor                     = UIColor.darkGray
    
    var segmentBackgroundColor: UIColor             = UIColor(red: 101/255, green: 103/255, blue: 107/255, alpha: 1.0)
    var segmentTextColor: UIColor                   = UIColor.white
    var segmentSelectedBackgroundColor: UIColor     = UIColor(red: 50/255, green: 52/255, blue: 54/255, alpha: 1.0)
    var segmentSelectedTextColor: UIColor           = UIColor.white
    
    var progressViewBackgroundColor: UIColor        = UIColor.darkGray
    var progressViewTrackTintColor: UIColor         = TAppColor.greenColor
    var keyboardSeparatorColor: UIColor             = UIColor.clear
    var tableViewSeparatorColor: UIColor            = UIColor.darkGray
    var isStatusBarLightStyle: Bool                 = true
    var tabbarItemUnselectedColor: UIColor          = UIColor.lightGray
    
    var thumbColor: UIColor                         = UIColor.black
    var hightlightColor: UIColor                    = UIColor(red: 235.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
    var tableViewAccessoryColor: UIColor            = UIColor.darkGray
}

private class AppThemeLightGray: NSObject, AppThemeInterface {
    internal static let subColor = UIColor(red: 36/255.0, green: 37/255.0, blue: 38/255.0, alpha: 1.0)
    internal static let mainColor = UIColor(red: 50/255.0, green: 52/255.0, blue: 54/255.0, alpha: 1.0)
    
    var key: String                                 = "AppThemeLightGray"
    var navigationBackgroundColor: UIColor          = mainColor
    var navigationItemTintColor: UIColor            = TAppColor.greenColor
    var backgroundColor: UIColor                    = mainColor
    
    var titleColor: UIColor                         = UIColor.white
    var subTitleColor: UIColor                      = UIColor(red: 212/255.0, green: 212/255.0, blue: 212/255.0, alpha: 1.0)
    
    var tableBackgroundColor: UIColor               = subColor
    var tableBackgroundColor2: UIColor              = mainColor
    var tableCellBackgroundColor: UIColor           = mainColor
    var tableCellTitleColor: UIColor                = UIColor.white
    var tableCellSubTitleColor: UIColor             = UIColor.lightGray
    
    var calendarDayBackgroundInMonth: UIColor       = UIColor(red: 50/255.0, green: 52/255.0, blue: 54/255.0, alpha: 1.0)
    var calendarDayBackgroundNotInMonth: UIColor    = subColor
    var calendarBackgroundToday: UIColor            = UIColor.white.withAlphaComponent(0.2)
    
    var searchViewBackgroundColor: UIColor          = UIColor.darkGray
    var searchViewTextColor: UIColor                = UIColor.white
    var searchViewPlaceholderColor: UIColor         = UIColor.lightGray
    var separatorColor: UIColor                     = UIColor.darkGray
    
    var segmentBackgroundColor: UIColor             = UIColor(red: 101/255, green: 103/255, blue: 107/255, alpha: 1.0)
    var segmentTextColor: UIColor                   = UIColor.white
    var segmentSelectedBackgroundColor: UIColor     = UIColor(red: 50/255, green: 52/255, blue: 54/255, alpha: 1.0)
    var segmentSelectedTextColor: UIColor           = UIColor.white
    
    var progressViewBackgroundColor: UIColor        = UIColor.darkGray
    var progressViewTrackTintColor: UIColor         = TAppColor.greenColor
    var keyboardSeparatorColor: UIColor             = UIColor.clear
    var tableViewSeparatorColor: UIColor            = UIColor.darkGray
    var isStatusBarLightStyle: Bool                 = true
    var tabbarItemUnselectedColor: UIColor          = UIColor.lightGray
    //UIColor(red: 101/255, green: 103/255, blue: 107/255, alpha: 1.0)
    
    var thumbColor: UIColor                         = UIColor(red: 101/255, green: 103/255, blue: 107/255, alpha: 1.0)
    var hightlightColor: UIColor                    = UIColor(red: 235.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
    var tableViewAccessoryColor: UIColor            = UIColor.darkGray
}

private class AppThemeDarkBlue: NSObject, AppThemeInterface {
    internal static let mainColor   = UIColor(red: 27/255.0, green: 38/255.0, blue: 49/255.0, alpha: 1.0)
//    internal static let mainColor   = UIColor(red: 36/255.0, green: 51/255.0, blue: 71/255.0, alpha: 1.0)
    internal static let tableColor  = UIColor(red: 36/255.0, green: 51/255.0, blue: 71/255.0, alpha: 1.0)
    internal static let separator   = UIColor(red: 55/255.0, green: 73/255.0, blue: 91/255.0, alpha: 1.0)
    
    var key: String                                 = "AppThemeDarkBlue"
    var navigationBackgroundColor: UIColor          = mainColor
    var navigationItemTintColor: UIColor            = UIColor.white
    var backgroundColor: UIColor                    = mainColor
    var titleColor: UIColor                         = UIColor.white
    var subTitleColor: UIColor                      = UIColor(red: 157/255.0, green: 173/255.0, blue: 187/255.0, alpha: 1.0)
    
    var tableBackgroundColor: UIColor               = UIColor.hexColor("#37495B")
    var tableBackgroundColor2: UIColor              = mainColor
    var tableCellBackgroundColor: UIColor           = mainColor
    var tableCellTitleColor: UIColor                = UIColor.white
    var tableCellSubTitleColor: UIColor             = UIColor.lightGray
    
    var calendarDayBackgroundInMonth: UIColor       = mainColor
    var calendarDayBackgroundNotInMonth: UIColor    = tableColor
    var calendarBackgroundToday: UIColor            = UIColor.systemGreen.withAlphaComponent(0.2)
    
    var searchViewBackgroundColor: UIColor          = tableColor
    var searchViewTextColor: UIColor                = UIColor.white
    var searchViewPlaceholderColor: UIColor         = UIColor(red: 157/255.0, green: 173/255.0, blue: 187/255.0, alpha: 1.0)
    var separatorColor: UIColor                     = separator
    
    var segmentBackgroundColor: UIColor             = separator
    var segmentTextColor: UIColor                   = UIColor.white
    var segmentSelectedBackgroundColor: UIColor     = UIColor(red: 38/255.0, green: 58/255.0, blue: 81/255.0, alpha: 1.0)
    var segmentSelectedTextColor: UIColor           = UIColor.white
    
    var progressViewBackgroundColor: UIColor        = tableColor
    var progressViewTrackTintColor: UIColor         = TAppColor.greenColor
    var keyboardSeparatorColor: UIColor             = UIColor.clear
    var tableViewSeparatorColor: UIColor            = separator
    var isStatusBarLightStyle: Bool                 = true
    var tabbarItemUnselectedColor: UIColor          = UIColor.hexColor("9DADBB")
    
    var thumbColor: UIColor                         = UIColor(red: 73/255.0, green: 104/255.0, blue: 135/255.0, alpha: 1.0)
    var hightlightColor: UIColor                    = UIColor(red: 157/255.0, green: 173/255.0, blue: 187/255.0, alpha: 1.0)
    var tableViewAccessoryColor: UIColor            = separator
}
