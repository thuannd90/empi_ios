//
//  ThemeTableView.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SnapKit

class ThemeTableView: UITableView {

    let noDataView: UIView = UIView()
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setUpTheme()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpTheme()
    }
    
    func setUpTheme() {
        self.backgroundColor = theme.tableBackgroundColor
        UITableView.appearance().separatorColor = theme.tableViewSeparatorColor
    }
    
    override func reloadData() {
        super.reloadData()
        doRefreshNoDataView()
    }
    
    private func doRefreshNoDataView() {
        guard let source = self.dataSource else {
            doShowNoDataView()
            return
        }
               
        if let sectionCount = source.numberOfSections?(in: self) {
            if sectionCount == 0 {
                doShowNoDataView()
            } else {
                hideNodataView()
            }
        } else {
            let cellCount = source.tableView(self, numberOfRowsInSection: 0)
            if cellCount == 0 {
                doShowNoDataView()
            } else {
                hideNodataView()
            }
        }
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        doRefreshNoDataView()
    }
    
    private func doShowNoDataView() {
        noDataView.frame = self.bounds
        self.backgroundView = noDataView
        self.backgroundView?.isHidden = false
    }
    
    private  func hideNodataView() {
        self.backgroundView?.isHidden = true
        self.backgroundView = nil
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        doRefreshNoDataView()
    }
}
