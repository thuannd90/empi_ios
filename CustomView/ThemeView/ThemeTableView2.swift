//
//  ThemeTableView2.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ThemeTableView2: ThemeTableView {

    override func setUpTheme() {
        self.backgroundColor = theme.tableBackgroundColor2
        UITableView.appearance().separatorColor = theme.tableViewSeparatorColor
    }
}

extension UITableView {
    func getFooterViewBlank() -> UITableViewCell? {
        if let cell = self.dequeueReusableCell(withIdentifier: "footer") {
            return cell
        }
        
        self.register(ThemeBackgroundTableViewCell.classForCoder(), forCellReuseIdentifier: "footer")
        return self.dequeueReusableCell(withIdentifier: "footer")
    }
}
