//
//  ThemeImageView.swift
//  eMPI 2020
//
//  Created by Macintosh on 8/11/20.
//  Copyright © 2020 eMPI. All rights reserved.
//

import UIKit

class ThemeImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }
    
    func setUpView() {
        self.backgroundColor = UIColor.hexColor("#9DADBB")
    }
}
