//
//  ThemeProgressView.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ThemeProgressView: UIProgressView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTheme()
    }
          
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpTheme()
    }
          
    private func setUpTheme() {
        self.backgroundColor    = theme.progressViewBackgroundColor
        self.progressTintColor  = theme.progressViewTrackTintColor
    }
}
