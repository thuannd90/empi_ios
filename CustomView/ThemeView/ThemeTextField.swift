//
//  ThemeTextField.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ThemeTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTheme()
    }
       
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpTheme()
    }
       
    private func setUpTheme() {
        let att =  NSAttributedString(string: self.placeholder ?? "",
                                      attributes: [NSAttributedString.Key.foregroundColor: theme.searchViewPlaceholderColor])
        self.attributedPlaceholder = att
        self.textColor = theme.searchViewTextColor
        
        if theme.isStatusBarLightStyle {
            self.keyboardAppearance = .dark
        } else {
            self.keyboardAppearance = .light
        }
    }
}
