//
//  ThemeButton.swift
//  Utils
//
//  Created by Macintosh on 8/7/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ThemeButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTheme()
    }
    

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpTheme()
    }
    
    func setUpTheme() {
        self.tintColor = theme.navigationItemTintColor
    }
}
