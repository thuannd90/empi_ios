//
//  ThemeTableViewCell.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ThemeTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setUpView() {
        self.selectionStyle = .none
        self.backgroundColor                = theme.tableCellBackgroundColor
        self.contentView.backgroundColor    = theme.tableCellBackgroundColor
        
        if self.accessoryType == .disclosureIndicator {
            let image = UIImage(named: "ic_separator")?.withRenderingMode(.alwaysTemplate)
            let imageView = UIImageView(image: image)
            imageView.tintColor = theme.tableViewAccessoryColor
            imageView.contentMode = .center
            self.accessoryView = imageView
        }
        
        self.textLabel?.textColor       = theme.titleColor
        self.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
        
        self.detailTextLabel?.textColor = theme.subTitleColor
        self.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.0)
    }
    
    private weak var disclosureButton: UIButton? {
        didSet {
            if let originalImage = self.disclosureButton?.backgroundImage(for: .normal) {
                let templateImage = originalImage.withRenderingMode(.alwaysTemplate)
                self.disclosureButton?.setBackgroundImage(templateImage, for: .normal)
                self.disclosureButton?.setBackgroundImage(templateImage, for: .highlighted)
                self.disclosureButton?.setBackgroundImage(templateImage, for: .disabled)
                self.disclosureButton?.setBackgroundImage(templateImage, for: .selected)
                self.disclosureButton?.setBackgroundImage(templateImage, for: .application)
                self.disclosureButton?.setBackgroundImage(templateImage, for: .reserved)
            }
        }
    }
}
