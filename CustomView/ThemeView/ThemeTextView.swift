//
//  ThemeTextView.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SnapKit

class ThemeTextView: UITextView {

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setUpTheme()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpTheme()
    }
    
    var placeHodler: UILabel = UILabel()
    
    private func setUpTheme() {
        self.backgroundColor = UIColor.clear
        self.textColor = theme.searchViewTextColor
        
        if theme.isStatusBarLightStyle {
            self.keyboardAppearance = .dark
        } else {
            self.keyboardAppearance = .light
        }
        
        placeHodler.isHidden = !self.text.isEmpty
        self.addSubview(placeHodler)
        self.textContainerInset = UIEdgeInsets.zero
        self.textContainer.lineFragmentPadding = 0
        
        placeHodler.textColor = theme.searchViewPlaceholderColor
        placeHodler.text = TLocalizationStringHelper._note  + "..."
        placeHodler.font = self.font
        
        weak var weakSelf = self
        placeHodler.snp.makeConstraints { (maker) in
            maker.edges.topMargin.equalTo(4)
            maker.edges.leftMargin.equalTo(0)
            
            if let font = weakSelf?.font {
                maker.edges.height.equalTo(font.lineHeight)
            } else {
                maker.edges.height.equalTo(24)
            }
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidChangeNotification),
                                               name: UITextView.textDidChangeNotification,
                                               object: nil)
    }
    
    @objc func textDidChangeNotification() {
        placeHodler.isHidden = !self.text.isEmpty
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidChangeNotification, object: nil)
    }
}
