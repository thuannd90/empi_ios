//
//  ThemeSegmentView.swift
//  Utils
//
//  Created by Macintosh on 4/26/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ThemeSegmentView: UISegmentedControl {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTheme()
    }
          
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpTheme()
    }
          
    private func setUpTheme() {
        let attNormal = [NSAttributedString.Key.foregroundColor : theme.segmentTextColor]
        self.setTitleTextAttributes(attNormal, for: UIControl.State.normal)
        self.backgroundColor = theme.segmentBackgroundColor
        
        if #available(iOS 13.0, *) {
            self.selectedSegmentTintColor = theme.segmentSelectedBackgroundColor
            
            let attSelected = [NSAttributedString.Key.foregroundColor : theme.segmentSelectedTextColor]
            self.setTitleTextAttributes(attSelected, for: UIControl.State.selected)
        } else {
            self.tintColor = theme.navigationItemTintColor
            let attSelected = [NSAttributedString.Key.foregroundColor : UIColor.white]
            self.setTitleTextAttributes(attSelected, for: UIControl.State.selected)
        }
    }
}
