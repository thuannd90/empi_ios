//
//  TProgressHub.swift
//  Utils
//
//  Created by Macintosh on 9/28/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import UIKit
import SVProgressHUD

class TProgressHub: NSObject {
    class func show() {
        SVProgressHUD.show(withStatus: nil)
    }
    
    class func showProgressWithStr(_ str: String?) {
        SVProgressHUD.show(withStatus: str)
    }
    
    class func hide() {
//        SVProgressHUD.dismiss()
        SVProgressHUD.dismiss(withDelay: 0.5)
    }
    
    class func showErrWithStr(_ str: String?) {
        SVProgressHUD.showError(withStatus: str)
    }
    
    class func showError(_ err: Error?) {
        SVProgressHUD.showError(withStatus: err?.localizedDescription)
    }
}
