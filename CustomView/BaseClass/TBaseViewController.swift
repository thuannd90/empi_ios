//
//  TBaseViewController.swift
//  Utils
//
//  Created by Macintosh on 9/18/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import UIKit

class TBaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = theme.backgroundColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.hideKeyboard()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if theme.isStatusBarLightStyle {
            return .lightContent
        }
        
        if #available(iOS 13.0, *) {
            return UIStatusBarStyle.darkContent
        }
        
        return .default
    }
    
    func doDeinit() {
        
    }
    
    deinit {
    }
}
