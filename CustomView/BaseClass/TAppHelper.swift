//
//  StoreHelper.swift
//  BaseLife
//
//  Created by iMac on 1/14/20.
//  Copyright © 2020 BaseLife. All rights reserved.
//

import UIKit
import StoreKit

class TAppHelper: NSObject {
    
    static func rateApp() {
        let appId = "1502054514"
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + appId) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static func feedback() {
        let urlStr = "https://docs.google.com/spreadsheets/d/1mX2yvzhfsHfev1z0iYFYVcmm0cfudOS0Ib-wSChQYIQ/edit?usp=sharing"
        if let url = URL(string: urlStr) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static func contactUs() {
        let urlStr = "https://www.facebook.com/Money-Note-112364990386152"
        if let url = URL(string: urlStr) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static func shareApp(_ controller: UIViewController, _ position: CGRect) {
        let urlStr = "itms-apps://itunes.apple.com/app/1502054514"
        if let url = URL(string: urlStr) {
            let items = ["Money note", url] as [Any]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
            if isIphoneApp() {
                controller.present(ac, animated: true)
            } else {
                if let popOver = ac.popoverPresentationController {
                    popOver.sourceView = controller.view
                    popOver.sourceRect = position
                    popOver.permittedArrowDirections = .any
                    controller.present(ac, animated: true)
                }
            }
        }
    }
    
    static var appVersion: String {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let version = appVersion ?? ""
        return "\(TLocalizationStringHelper.version) \(version) (\(buildNumber))"
    }
    
    private static var buildNumber: String {
        let appVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        return appVersion ?? ""
    }
}
