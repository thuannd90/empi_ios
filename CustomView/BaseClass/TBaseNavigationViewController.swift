//
//  TBaseNavigationViewController.swift
//  Utils
//
//  Created by Macintosh on 9/18/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import UIKit

class TBaseNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        self.navigationBar.isOpaque = true
        self.delegate = self
        
        self.navigationBar.isOpaque = true
        self.navigationBar.isTranslucent = false
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0),
                                                  NSAttributedString.Key.foregroundColor: theme.titleColor]
        
        self.navigationBar.tintColor    = theme.navigationItemTintColor
        self.navigationBar.barTintColor = theme.navigationBackgroundColor
        self.navigationBar.shadowImage  = UIImage()
        self.interactivePopGestureRecognizer?.isEnabled = true
        self.interactivePopGestureRecognizer?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if theme.isStatusBarLightStyle {
            return .lightContent
        }
        
        if #available(iOS 13.0, *) {
            return UIStatusBarStyle.darkContent
        }
        
        return .default
    }
}

extension TBaseNavigationViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let currentVC = self.topViewController {
            let itemBack = UIBarButtonItem(title: "", style: .done, target: currentVC, action: nil)
            currentVC.navigationItem.backBarButtonItem = itemBack
        }
    }
}

extension TBaseNavigationViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
        return self.viewControllers.count > 1
    }
}
