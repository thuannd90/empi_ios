//
//  TBaseNavigationItem.swift
//  Utils
//
//  Created by Macintosh on 9/20/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import UIKit

enum AppNavigationItemStyle {
    case more
    case add
    case search
}

class TBaseNavigationItem: UIBarButtonItem {
    
    class func itemWithStyle(style: AppNavigationItemStyle, target: AnyObject, sel: Selector) -> UIBarButtonItem {
        switch style {
        case .more:
            return self.itemWithImage(image: #imageLiteral(resourceName: "ic_more"), target: target, action: sel)
        case .add:
            return self.itemWithImage(image: #imageLiteral(resourceName: "ic_plus"), target: target, action: sel)
        case .search:
            return self.itemWithImage(image: #imageLiteral(resourceName: "ic_search"), target: target, action: sel)
        }
    }
    
    private static func itemWithTitle(title: String, target: AnyObject, action: Selector) -> UIBarButtonItem {
        let item = UIBarButtonItem(title: title, style: .plain, target: target, action: action)
        return item
    }
    
    private static func itemWithImage(image: UIImage?, target: AnyObject, action: Selector) -> UIBarButtonItem {
        let item = UIBarButtonItem(image: image, style: .plain, target: target, action: action)
        return item
    }
}
