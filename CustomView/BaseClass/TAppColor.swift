//
//  TAppColor.swift
//  Utils
//
//  Created by Macintosh on 9/20/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import UIKit

class TAppColor: NSObject {
    static var greenColor = UIColor(red: 97.0/255.0, green: 173.0/255.0, blue: 101.0/255.0, alpha: 1.0)
    static var blueColor = UIColor.hexColor("2F80ED")
    static var redColor = UIColor(red: 237/255.0, green: 77/255.0, blue: 61/255.0, alpha: 1.0)
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
