//
//  TCalendarView.swift
//  BaseLife
//
//  Created by Macintosh on 12/3/19.
//  Copyright © 2019 BaseLife. All rights reserved.
//

import UIKit
import FSCalendar

protocol TCalendarViewDelegate {
    func calendar(_ calendar: TCalendarView, boundingRectWillChange bounds: CGRect, animated: Bool)
    func calendar(_ calendar: TCalendarView, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
}

class TCalendarView: UIView {

    var delegate: TCalendarViewDelegate?
    var calendarView: FSCalendar = FSCalendar()
    
    @IBInspectable var IsFullStyle: Bool = false
    
    fileprivate let bottomView: UIView = UIView()
    fileprivate var headerView: TCalendarHeaderView?
    
    lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarView,
                                                action: #selector(self.calendarView.handleScopeGesture(_:)))
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    lazy var scopeTableViewGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarView,
                                                action: #selector(self.calendarView.handleScopeGesture(_:)))
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    func doShowFull() {
        perform( #selector(self.calendarView.handleScopeGesture(_:)))
    }
    
    var scope: FSCalendarScope {
        get {
            return calendarView.scope
        }
        set {
            calendarView.scope = newValue
        }
    }
    
    var preScope: FSCalendarScope = FSCalendarScope.month
    
    func setDate(_ date: Date, _ animation: Bool) {
        calendarView.select(date)
        calendarView.setCurrentPage(date, animated: animation)
    }
    
    var selectedDate: Date? {
        get {
            return calendarView.selectedDate
        }
    }
    
    var selectedDates: [Date] {
        get {
            return calendarView.selectedDates
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        setUpData()
        reloadEvents()
        updateTodayButtonStatus()
        updateMonthLabel()
        setUpHeaderView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
        setUpData()
        reloadEvents()
        updateTodayButtonStatus()
        updateMonthLabel()
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        self.setUpHeaderView()
    }
    
    func setUpHeaderView() {
        if headerView == nil {
            if self.IsFullStyle {
                headerView = TCalendarHeaderView.initWithNibName("TCalendarHeaderViewFull")
            } else {
                headerView = TCalendarHeaderView.initWithDefaultNib()
            }
            
            self.addSubview(headerView!)
            headerView?.snp.makeConstraints { (maker) in
                maker.topMargin.equalToSuperview()
                maker.leftMargin.equalToSuperview()
                maker.rightMargin.equalToSuperview()
                maker.height.equalTo(70.0)
            }
            headerView?.btnToday.addTarget(self, action: #selector(btnTodayPressed), for: .touchUpInside)
        }
    }
    
    func setUpView() {
        self.clipsToBounds = true
        
        self.addSubview(bottomView)
        bottomView.snp.makeConstraints { (maker) in
            maker.topMargin.equalTo(70.0)
            maker.leftMargin.equalToSuperview()
            maker.rightMargin.equalToSuperview()
            maker.bottomMargin.equalToSuperview()
        }
        self.addSubview(bottomView)
        
        bottomView.addSubview(calendarView)
        calendarView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        self.calendarView.register(TCalendarCollectionViewCell.self, forCellReuseIdentifier: "cell")
        self.calendarView.dataSource = self
        self.calendarView.headerHeight = 0
        self.calendarView.weekdayHeight = 0
        self.calendarView.firstWeekday = 2
        self.calendarView.appearance.selectionColor = TAppColor.greenColor
        self.calendarView.appearance.todayColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
        self.calendarView.appearance.titleFont = UIFont.systemFont(ofSize: 20, weight: .regular)
        self.calendarView.appearance.weekdayTextColor = UIColor.darkGray
        self.calendarView.appearance.borderRadius = 0.3
    }
    
    func setUpData() {
        self.calendarView.scope = .month 
        self.calendarView.dataSource = self
        self.calendarView.delegate = self
        self.calendarView.locale = Locale(identifier: "vi")
        self.calendarView.select(Date(), scrollToDate: true)
        preScope = self.calendarView.scope
    }
    
    func reloadEvents() {
        
    }
    
    private func updateTodayButtonStatus() {
        if calendarView.scope == .month {
            if calendarView.currentPage.isInThisMonth {
                if let date = calendarView.selectedDate  {
                    headerView?.btnToday.isHidden = date.isInToday
                } else {
                    headerView?.btnToday.isHidden = false
                }
                
            } else {
                headerView?.btnToday.isHidden = false
            }
        } else {
            if calendarView.currentPage.isInThisWeek {
                if let date = calendarView.selectedDate {
                    headerView?.btnToday.isHidden = date.isInToday
                } else {
                    headerView?.btnToday.isHidden = false
                }
            } else {
                headerView?.btnToday.isHidden = false
            }
        }
    }
    
    func updateMonthLabel() {
        let date = self.calendarView.currentPage
        headerView?.lblTitle.text = date.getSolarMonthStr.capitalized + " - " + "\(date.getSolarYear)"
        
        if preScope != self.scope {
            preScope = self.scope
            
//            weak var weakSelf = self
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
//                weakSelf?.calendarView.select(date, scrollToDate: true)
//                weakSelf?.calendarView.reloadData()
//                weakSelf?.headerView.btnToday.isHidden = date.isInToday
//            })
        }
    }
    
    @objc func btnTodayPressed() {
        let date = Date()
        self.calendarView.select(date, scrollToDate: true)
        delegate?.calendar(self, didSelect: date, at: FSCalendarMonthPosition.current)
        updateTodayButtonStatus()
        updateMonthLabel()
        self.calendarView.reloadData()
    }
}

extension TCalendarView: FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        if let cell = cell as? TCalendarCollectionViewCell {
            var isSelected: Bool = false
            
            if let selected = calendar.selectedDate,
                selected.getSolarYear == date.getSolarYear,
                selected.getSolarMonth == date.getSolarMonth,
                selected.getSolarDay == date.getSolarDay {
                isSelected = true
            }
            cell.setUpView(date, position, isSelected, IsFullStyle)
        }
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 0
    }
}

extension TCalendarView: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        var rect = bounds
        let height = rect.size.height + bottomView.frame.origin.y
        rect.size.height = height
        delegate?.calendar(self, boundingRectWillChange: rect, animated: animated)
        updateMonthLabel()
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        delegate?.calendar(self, didSelect: date, at: monthPosition)
        updateTodayButtonStatus()
        updateMonthLabel()
        calendar.reloadData()
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        updateTodayButtonStatus()
        updateMonthLabel()
    }
}
