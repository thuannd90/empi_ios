//
//  TCalendarContentView.swift
//  BaseLife
//
//  Created by Macintosh on 12/18/19.
//  Copyright © 2019 BaseLife. All rights reserved.
//

import UIKit
import FSCalendar

class TCalendarContentView: UIView {

    @IBOutlet weak var mainView: UIView?
    @IBOutlet weak var lblSolarDay: UILabel?
    @IBOutlet weak var imvEvent: UIView?
    
    func setUpView(_ date: Date, _ position: FSCalendarMonthPosition, _ isSelected: Bool) {
        lblSolarDay?.text = "\(date.getSolarDay)"
        mainView?.backgroundColor = date.isInToday ? TAppColor.blueColor: UIColor.clear
        
        let deactiveColor = theme.subTitleColor.withAlphaComponent(0.5)
        let color = position == .current ? theme.titleColor : deactiveColor
        lblSolarDay?.textColor = color
        
        if date.isInToday {
            lblSolarDay?.textColor = .white
        } else {
            if position == .current {
                if date.isSunday {
                    lblSolarDay?.textColor = TAppColor.redColor
                }
            }
        }
        
        mainView?.layer.borderWidth = isSelected ? 1.5 : 0.0
        mainView?.layer.borderColor = TAppColor.blueColor.cgColor
    }
}
