//
//  TCalendarContentView.swift
//  BaseLife
//
//  Created by Macintosh on 12/18/19.
//  Copyright © 2019 BaseLife. All rights reserved.
//

import UIKit
import FSCalendar

class TCalendarContentViewFull: UIView {

    @IBOutlet weak var mainView: UIView?
    @IBOutlet weak var lblSolarDay: UILabel?
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var eventTableView: UITableView!
    private var currentDate: Date = Date()
    
    func setUpView(_ date: Date, _ position: FSCalendarMonthPosition, _ isSelected: Bool) {
        lblSolarDay?.text = "\(date.getSolarDay)"
        
        mainView?.layer.borderWidth = 1.0
        mainView?.layer.borderColor = theme.separatorColor.cgColor
        mainView?.backgroundColor = date.isWeekend ? theme.tableBackgroundColor.withAlphaComponent(0.3) : UIColor.clear
        
        if date.isInToday {
            lblSolarDay?.textColor = .white
            todayView.isHidden = false
        } else {
            let deactiveColor = theme.subTitleColor.withAlphaComponent(0.5)
            let color = position == .current ? theme.titleColor : deactiveColor
            lblSolarDay?.textColor = color
            todayView.isHidden = true
        }
        
        currentDate = date
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        superview?.willMove(toWindow: newWindow)
        eventTableView.dataSource = self
        eventTableView.delegate = self
        eventTableView.reloadData()
    }
}

extension TCalendarContentViewFull: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentDate.isInToday ? 8 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TCalendarEventTableViewCell.dequeCellWithTable(tableView)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DLog("select event")
    }
}
