//
//  TCalendarCollectionViewCell.swift
//  BaseLife
//
//  Created by Macintosh on 12/18/19.
//  Copyright © 2019 BaseLife. All rights reserved.
//

import UIKit
import FSCalendar

class TCalendarCollectionViewCell: FSCalendarCell {

    private var _contentViewSmall: TCalendarContentView?
    private var _contentViewFull: TCalendarContentViewFull?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setUpView(_ date: Date, _ position: FSCalendarMonthPosition, _ isSelected: Bool, _ isFull: Bool) {
        self.titleLabel.isHidden    = true
        self.subtitleLabel.isHidden = true
        self.shapeLayer.isHidden    = true
        
        if isFull {
            if _contentViewFull == nil {
                let view = TCalendarContentViewFull.initWithDefaultNib()
                self.addSubview(view)
                
                view.snp.makeConstraints { (maker) in
                    maker.edges.equalToSuperview()
                }
                
                _contentViewFull = view
            }
            
            _contentViewFull?.setUpView(date, position, isSelected)
            
            _contentViewSmall?.removeFromSuperview()
            _contentViewSmall = nil
            
        } else {
            if _contentViewSmall == nil {
                let view = TCalendarContentView.initWithDefaultNib()
                self.addSubview(view)
                
                view.snp.makeConstraints { (maker) in
                    maker.edges.equalToSuperview()
                }
                
                _contentViewSmall = view
            }
            
            _contentViewSmall?.setUpView(date, position, isSelected)
            
            _contentViewFull?.removeFromSuperview()
            _contentViewFull = nil
        }
    }
}
