//
//  TCalendarHeaderView.swift
//  BaseLife
//
//  Created by Macintosh on 12/3/19.
//  Copyright © 2019 BaseLife. All rights reserved.
//

import UIKit

class TCalendarHeaderView: UIView {

    @IBOutlet weak var btnToday: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
}
